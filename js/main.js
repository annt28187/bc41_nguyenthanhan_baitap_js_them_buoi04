// Bài 1
/*
Input: Nhập ngày hiện tại

Các bước thực hiện:
- Viết hàm kiểm tra năm nhuận
   + Nếu những năm chia hết cho 4 và không chia hết cho 100 hoặc những năm chia hết cho 400
- Viết hàm những ngày trong tháng
   + Nếu là tháng 4, 6, 9, 11 trả về 30 ngày
   + Nếu là tháng 2 và là năm nhuận trả về 29 ngày ngược lại 28 ngày
   + Còn lại là 31 ngày   
- Viết hàm Tính ngày mai
   + Tạo biến day, month, year
   + Cho người dùng nhập và gán vào day
   + Cho người dùng nhập và gán vào month
   + Cho người dùng nhập và gán vào year
   + Tạo biến và gán day1 = day, month1 = month, year1 = year
   + Kiểm tra giá trị nhập. Sai xuất hiện thông báo và nhập lại
     Nếu nhập đúng thì day1 = day + 1
     Nếu month != 12 và day == số ngày ngày tối đa trong tháng thì day1 = 1 và month1 = month+1;
     Nếu month == 12 và day == số ngày ngày tối đa trong tháng thì day1 = 1, month1 = 1 year1 = year + 1;
     Nếu month == 2 
      Nếu là năm nhuận và day == 29 thì day1 = 1 và month1 = month+1;
      Nếu không là năm nhuận à day == 28 thì day1 = 1 và month1 = month+1;
- Viết hàm Tính ngày hôm qua
   + Tạo biến day, month, year
   + Cho người dùng nhập và gán vào day
   + Cho người dùng nhập và gán vào month
   + Cho người dùng nhập và gán vào year
   + Tạo biến và gán day1 = day, month1 = month, year1 = year
   + Kiểm tra giá trị nhập. Sai xuất hiện thông báo và nhập lại
     Nếu nhập đúng thì day1 = day - 1
     Nếu month != 1 và day == 1 thì month1 = month - 1 và day1 = số ngày ngày tối đa trong tháng (month1, year);
     Nếu month == 1 và day == 1 thì year1 = year - 1,  month1 = 12  và day1 = số ngày ngày tối đa trong tháng (month1, year1);
     Nếu month == 3 
      Nếu là năm nhuận và day == 1 thì month1 = month-1 và day1 = số ngày ngày tối đa trong tháng (month1, year);
      Nếu không là năm nhuận à day == 1 thì month1 = month-1 và day1 = số ngày ngày tối đa trong tháng (month1, year);
  - Xuất kết quả  
Outout: Xuất ngày hôm qua và ngày mai
*/

function leapYear(y) {
  if ((y % 4 == 0 && y % 100 != 0) || y % 400 == 0) {
    return true;
  } else {
    return false;
  }
}

function dayInMonth(m, y) {
  switch (m) {
    case 4:
    case 6:
    case 9:
    case 11:
      return 30;
    case 2:
      if (leapYear(y)) {
        return 29;
      }
      return 28;
    default:
      return 31;
  }
}

function calTomorrow() {
  var day = document.getElementById("inputDay").value * 1,
    month = document.getElementById("inputMonth").value * 1,
    year = document.getElementById("inputYear").value * 1;
  var day1 = day,
    month1 = month,
    year1 = year;
  if (
    year > 0 &&
    month >= 1 &&
    month <= 12 &&
    day >= 1 &&
    day <= dayInMonth(month, year)
  ) {
    day1 = day + 1;
    if (month != 12 && day == dayInMonth(month, year)) {
      day1 = 1;
      month1 = month + 1;
    } else if (month == 12 && day == dayInMonth(month, year)) {
      day1 = 1;
      month1 = 1;
      year1 = year + 1;
    } else if (month == 2) {
      if (leapYear(year)) {
        if (day == 29) {
          day1 = 1;
          month1 = month + 1;
        }
      } else {
        if (day == 28) {
          day1 = 1;
          month1 = month + 1;
        }
      }
    }
    if (day1 < 10) day1 = "0" + day1;
    if (month1 < 10) month1 = "0" + month1;
    document.getElementById("txtDate").innerHTML = `${day1}/${month1}/${year1}`;
  } else {
    alert("Dữ liệu không hợp lệ");
  }
}

function calYesterday() {
  var day = document.getElementById("inputDay").value * 1,
    month = document.getElementById("inputMonth").value * 1,
    year = document.getElementById("inputYear").value * 1;
  var day1 = day,
    month1 = month,
    year1 = year;
  if (
    year > 0 &&
    month >= 1 &&
    month <= 12 &&
    day >= 1 &&
    day <= dayInMonth(month, year)
  ) {
    day1 = day - 1;
    if (month != 1 && day == 1) {
      month1 = month - 1;
      day1 = dayInMonth(month1, year);
    } else if (month == 1 && day == 1) {
      year1 = year - 1;
      month1 = 12;
      day1 = dayInMonth(month1, year1);
    } else if (month == 3) {
      if (leapYear(year)) {
        if (day == 1) {
          month1 = month - 1;
          day1 = dayInMonth(month1, year);
        }
      } else {
        if (day == 1) {
          month1 = month - 1;
          day1 = dayInMonth(month1, year);
        }
      }
    }
    if (day1 < 10) day1 = "0" + day1;
    if (month1 < 10) month1 = "0" + month1;
    document.getElementById("txtDate").innerHTML = `${day1}/${month1}/${year1}`;
  } else {
    alert("Dữ liệu không hợp lệ");
  }
}

// Bài 2
/*
Input: Nhập tháng và năm

Các bước thực hiện:
- Viết hàm kiểm tra năm nhuận
   + Sử dụng lại hàm đã viết ở Bài 1
- Viết hàm những ngày trong tháng
   +Sử dụng lại hàm đã viết ở Bài 1
- Viết hàm Tính ngày
   + Tạo biến  month, year
   + Cho người dùng nhập và gán vào month
   + Cho người dùng nhập và gán vào year
   + Tạo biến day
   + Kiểm tra giá trị nhập. Sai xuất hiện thông báo và nhập lại
     Nếu nhập đúng thì day = số ngày ngày tối đa trong tháng (month, year)
 - Xuất kết quả  
    
Outout: Xuất ngày cúa tháng và năm 
*/

function calDay() {
  var month = document.getElementById("inputMonth2").value * 1,
    year = document.getElementById("inputYear2").value * 1;
  var day;
  if (year > 0 && month >= 1 && month <= 12) {
    day = dayInMonth(month, year);
    document.getElementById(
      "txtCalcDay"
    ).innerHTML = `Tháng ${month} năm ${year} có ${day}`;
  } else {
    alert("Dữ liệu không hợp lệ");
  }
}

// Bài 3
/*
Input: Nhập số nguyên gồm 3 chữ số

Các bước thực hiện:
- Tạo biến number, hundred, ten, unit, i, j, t
- Cho người dùng nhập và gán vào number
- Gán giá trị lần lượt là
   + hundred = Math.floor(number / 100)
   + ten = Math.floor((number % 100) / 10)
   + unit = (number % 100) % 10
   + i = "",
   + j = "", 
   + t = "";
- Viết switch case cho hàm trăm (chạy từ 1 đến 9)
- Viết switch case cho hàm chục (chạy từ 0 đến 9)
- Viết switch case cho hàm đơn vị (chạy từ 0 đến 9)
- Xuất kết quả  
    
Outout: Xuất ngày hôm qua và ngày mai
*/

function readInt() {
  var number = document.getElementById("inputReadInt").value * 1,
    hundred = Math.floor(number / 100),
    ten = Math.floor((number % 100) / 10),
    unit = (number % 100) % 10;
  (i = ""), (j = ""), (t = "");
  if (hundred > 0) {
    switch (hundred) {
      case 1:
        i = "một trăm ";
        break;
      case 2:
        i = "hai trăm ";
        break;
      case 3:
        i = "ba trăm ";
        break;
      case 4:
        i = "bốn trăm ";
        break;
      case 5:
        i = "năm trăm ";
        break;
      case 6:
        i = "sáu trăm ";
        break;
      case 7:
        i = "bảy trăm ";
        break;
      case 8:
        i = "tám trăm ";
        break;
      case 9:
        i = "chín trăm ";
        break;
      default:
        alert("số hàng trăm không xác định được");
    }
    switch (ten) {
      case 0:
        j = "lẻ ";
        break;
      case 1:
        j = "mười ";
        break;
      case 2:
        j = "hai mươi ";
        break;
      case 3:
        j = "ba mươi ";
        break;
      case 4:
        j = "bốn mươi ";
        break;
      case 5:
        j = "năm mươi ";
        break;
      case 6:
        j = "sáu mươi ";
        break;
      case 7:
        j = "bảy mươi ";
        break;
      case 8:
        j = "tám mươi ";
        break;
      case 9:
        j = "chín mươi ";
        break;
      default:
        alert("Số hàng chục không xác định được");
    }
    switch (unit) {
      case 0:
        t = "";
        break;
      case 1:
        t = "một";
        break;
      case 2:
        t = "hai";
        break;
      case 3:
        t = "ba";
        break;
      case 4:
        t = "bốn";
        break;
      case 5:
        t = "năm";
        break;
      case 6:
        t = "sáu";
        break;
      case 7:
        t = "bảy";
        break;
      case 8:
        t = "tám";
        break;
      case 9:
        t = "chín";
        break;
      default:
        alert("Số hàng đơn vị không xác định được.");
    }
    document.getElementById("txtReadInt").innerHTML = i + j + t;
  } else {
    alert("Dữ liệu không hợp lệ");
  }
}

// Bài 4
/*
Input: Nhập tọa độ 3 sinh viên và tọa độ của trường học

Các bước thực hiện:
- Tạo biến name1, x1, y1, name2, x2, y2, name3, x3, y3, x4, y4
- Cho người dùng nhập và gán vào name1
- Cho người dùng nhập và gán vào x1
- Cho người dùng nhập và gán vào y1
- Cho người dùng nhập và gán vào name2
- Cho người dùng nhập và gán vào x2
- Cho người dùng nhập và gán vào y2
- Cho người dùng nhập và gán vào name3
- Cho người dùng nhập và gán vào x3
- Cho người dùng nhập và gán vào y3
- Cho người dùng nhập và gán vào x4
- Cho người dùng nhập và gán vào y4
+ Kiểm tra giá trị của các tọa độ nhập (tọa độ >= 0). Sai xuất hiện thông báo và nhập lại
    Nếu nhập đúng tạo và gán giá trị
    r1 = Math.pow(x4 - x1, 2) + Math.pow(y4 - y1, 2),
      d1 = Math.sqrt(r1);
    r2 = Math.pow(x4 - x2, 2) + Math.pow(y4 - y2, 2),
      d2 = Math.sqrt(r2);
    r3 = Math.pow(x4 - x3, 2) + Math.pow(y4 - y3, 2),
      d3 = Math.sqrt(r3);
- Tạo biến result để lưu kết quả
- Kiểm tra các giá trị d1, d2, d3 coi giá trì nào lớn nhất
- Xuất kết quả  
    
Outout: Xuất simh viên xa nhất
*/

function search() {
  var name1 = document.getElementById("inputName1").value,
    x1 = document.getElementById("inputX1").value * 1,
    y1 = document.getElementById("inputY1").value * 1,
    name2 = document.getElementById("inputName2").value,
    x2 = document.getElementById("inputX2").value * 1,
    y2 = document.getElementById("inputY2").value * 1,
    name3 = document.getElementById("inputName3").value,
    x3 = document.getElementById("inputX3").value * 1,
    y3 = document.getElementById("inputY3").value * 1,
    x4 = document.getElementById("inputX4").value * 1,
    y4 = document.getElementById("inputY4").value * 1;
  if (
    x1 >= 0 &&
    y1 >= 0 &&
    x2 >= 0 &&
    y2 >= 0 &&
    x3 >= 0 &&
    y3 >= 0 &&
    x4 >= 0 &&
    y4 >= 0
  ) {
    var r1 = Math.pow(x4 - x1, 2) + Math.pow(y4 - y1, 2),
      d1 = Math.sqrt(r1);
    var r2 = Math.pow(x4 - x2, 2) + Math.pow(y4 - y2, 2),
      d2 = Math.sqrt(r2);
    var r3 = Math.pow(x4 - x3, 2) + Math.pow(y4 - y3, 2),
      d3 = Math.sqrt(r3);
    var result = "";
    (result = d1 > d2 && d1 > d3 ? name1 : d2 > d1 && d2 > d3 ? name2 : name3),
      (document.getElementById("txtSearch").innerHTML =
        "Sinh viên xa trường nhất: " + result);
  } else {
    alert("Dữ liệu không hợp lệ");
  }
}
